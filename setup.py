#!/usr/bin/env python

from __future__ import with_statement

from setuptools import setup

def get_readme():
    try:
        with open('README.rst') as f:
            return f.read().strip()
    except IOError:
        return ''

setup(
    name='easyirc',
    version='0.1.4',
    description='Easy IRC is an IRC toolkit to develop IRC client or bot, especially for Python/IRC beginner.',
    long_description=get_readme(),
    author='Martin Owens, was: Jeong YunWon',
    author_email='doctormo@gmail.com',
    url='https://github.com/doctormo/easyirc',
    packages=(
        'easyirc',
        'easyirc/command',
        'easyirc/event',
        'easyirc/client',
    ),
    test_suite='tests',
    install_requires=[
        'setuptools',
        'prettyexc',
    ],
)
